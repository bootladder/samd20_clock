#include "samd20.h"

int Clock_Init (void)
{
  //system_init();

  //CONFIGURE DFLL AND GCLKS HERE
  //DO NOT CONNECT A GCLK TO A PERIPHERAL, EXCEPT DFLL REFERENCE


  //Enable OSC8M, no prescaler
  SYSCTRL->OSC8M.reg |= SYSCTRL_OSC8M_ENABLE;
  SYSCTRL->OSC8M.bit.PRESC = 0;

  //SYSCTRL->DFLLCTRL.bit.ENABLE = true;
  ////DFLL step size, skipping
  ////multiplication factor
  //SYSCTRL->DFLLMUL.bit.MUL = 7000;
  //SYSCTRL->DFLLCTRL.bit.MODE = 1; //closed loop mode
  //

  //DFLL reference set to generator 1
  //GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SYSCTRL_GCLK_ID_DFLL48) |
  //          GCLK_CLKCTRL_CLKEN            |
  //          GCLK_CLKCTRL_GEN(1);
  ////Configure Generator 1, OSC8M divide by 1024
  //GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_GENCTRL_ID_GCLK1_Val) |
  //          GCLK_GENDIV_DIV(1024);
  ////Enable GCLK1
  //GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(GCLK_GENCTRL_ID_GCLK1_Val) |
  //          GCLK_GENCTRL_GENEN               |
  //          GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSC8M_Val) ;




  //Configure Generator 3, OSC8M divide by 1
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
            GCLK_GENDIV_DIV(1);
  //Enable GCLK3
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(GCLK_GENCTRL_ID_GCLK3_Val) |
            GCLK_GENCTRL_GENEN              |
            GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSC8M_Val) ;
  ////Connect GCLK3 to SERCOM4
  //GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(GCLK_CLKCTRL_ID_SERCOM4_CORE_Val) |
            //GCLK_CLKCTRL_GEN(GCLK_CLKCTRL_GEN_GCLK3) |
            //GCLK_CLKCTRL_CLKEN;

  return 0;
}

