##############################################################################
BUILD = build
PUBLIC= public
BIN = libSAMD20_Clock
##############################################################################
.PHONY: all directory clean size

CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size

CFLAGS += -W -Wall --std=gnu99 -O0
CFLAGS += -fdata-sections -ffunction-sections
CFLAGS += -funsigned-char -funsigned-bitfields
CFLAGS += -mcpu=cortex-m0plus -mthumb
CFLAGS += -MD -MP -MT $(BUILD)/$(*F).o -MF $(BUILD)/$(@F).d

LDFLAGS += -mcpu=cortex-m0plus -mthumb

INCLUDES += \
  -I../include \
  -I../samd20_cmsis_headers \
  -I../arm_cmsis_headers

DEFINES += \
  -D__SAMD20J18__ \
  -DDONT_USE_CMSIS_INIT \
  -DF_CPU=48000000

CFLAGS += $(INCLUDES) $(DEFINES)
OBJS = build/SAMD20_Clock.o 


all: directory libSAMD20_Clock.a size

libSAMD20_Clock.a: $(OBJS)
	@echo $(AR) -r -o $@ $(OBJS) 
	@$(AR) -r -o public/libSAMD20_Clock.a $(OBJS) 
	@cp SAMD20_Clock.h public/
	@cp SAMD20_Clock.h ../lib
	@cp public/libSAMD20_Clock.a ../lib

build/SAMD20_Clock.o: 
	@echo $(CC) $(CFLAGS) SAMD20_Clock.c -c -o $@
	@$(CC) $(CFLAGS) SAMD20_Clock.c -c -o $@

directory:
	@mkdir -p $(BUILD)
	@mkdir -p $(PUBLIC)

clean:
	@echo clean

