# SAMD20 Clock Library #

Configure your clock sources, GCLK generators in here, make it a library, forget about it, reuse it.
Do the final hooking up of the GCLK generators to whatever peripheral clocks, in your application.